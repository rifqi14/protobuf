package com.ljf.protobuf_android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tutorial.AddressBookProtos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by mr.lin on 2018/6/8.
 */

public class MainActivity extends Activity {

    private TextView resultTv;
    private Button readFileBt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        AddressBookProtos.Person Pak_Owi = AddressBookProtos.Person
                .newBuilder()
                .setName("Pak Owi")
                .setId(1)
                .setEmail("tantowijauhari@imani.com")
                .addPhones(0,
                        AddressBookProtos.Person.PhoneNumber.newBuilder()
                                .setNumber("1234")
                                .setType(AddressBookProtos.Person.PhoneType.HOME)
                                .build())
                .build();

        AddressBookProtos.Person Pak_Arif = AddressBookProtos.Person
                .newBuilder()
                .setName("Pak Arif")
                .setId(2)
                .setEmail("arif@imani.com")
                .addPhones(0,
                        AddressBookProtos.Person.PhoneNumber.newBuilder()
                                .setNumber("1234")
                                .setType(AddressBookProtos.Person.PhoneType.WORK)
                                .build())
                .build();

        AddressBookProtos.Person rifqi = AddressBookProtos.Person
                .newBuilder()
                .setName("Rifqi")
                .setId(2)
                .setEmail("Rifqi@imani.com")
                .addPhones(0,
                        AddressBookProtos.Person.PhoneNumber.newBuilder()
                                .setNumber("1234")
                                .setType(AddressBookProtos.Person.PhoneType.WORK)
                                .build())
                .build();

        AddressBookProtos.AddressBook addressBook = AddressBookProtos.AddressBook.newBuilder()
                .addPeople(0, Pak_Owi)
                .addPeople(1, Pak_Arif)
                .addPeople(2, rifqi)
                .build();
        try {
            FileOutputStream fileOutputStream = openFileOutput("proto", MODE_PRIVATE);
            fileOutputStream.write(addressBook.toByteArray());
            fileOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
            resultTv = findViewById(R.id.resultTv);
             readFileBt = findViewById(R.id.readFileBt);


        readFileBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream fileInputStream = openFileInput("proto");
                    AddressBookProtos.AddressBook addressBook = AddressBookProtos.AddressBook.parseFrom(fileInputStream);
                    resultTv.setText("from local:\n" + addressBook.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



    }



}